import xml.dom.minidom

class SettingsHandler:

    def __init__(self):
        self.read_settings()

    def read_settings(self):
        dom = xml.dom.minidom.parse("settings.xml")
        dom.normalize()

        node_local_catalog = dom.getElementsByTagName('local_path')[0]. \
            firstChild.data
        # print("xml " + node_local_catalog)

        node_remote_catalog = dom.getElementsByTagName('remote_path')[0]. \
            firstChild.data
        # print("xml " + node_remote_catalog)

        ftp_auth_data_list = dom.getElementsByTagName('ftp-auth-data')
        for item in ftp_auth_data_list:
            node_host = item.getElementsByTagName("host")[0].firstChild.data
            node_username = item.getElementsByTagName("username")[0].firstChild.data
            node_password = item.getElementsByTagName("password")[0].firstChild.data

        self.HOST = node_host
        self.USERNAME = node_username
        self.PASSWORD = node_password
        self.LOCAL_PATH = node_local_catalog
        self.REMOTE_PATH = node_remote_catalog
