from PostersUploader import PostersUploader
from SettingsHandler import SettingsHandler

# Uses Python 3.7

settings = SettingsHandler()
local_catalog = settings.LOCAL_PATH
remote_catalog = settings.REMOTE_PATH
ftp_auth_data = [settings.HOST, settings.USERNAME, settings.PASSWORD]

executor = PostersUploader(ftp_auth_data, local_catalog, remote_catalog)
executor.print_file_list()

executor.upload_files()
