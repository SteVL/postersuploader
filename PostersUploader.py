import os
from ftplib import FTP


class PostersUploader:

    def __init__(self, ftp_auth_data, local_catalog, remote_catalog):
        """Данные для FTP соединения(хост, логин, пароль), исходный каталог на локальной машине, удаленный целевой каталог"""
        self.ftp_auth_data = ftp_auth_data
        self.local_catalog = local_catalog
        self.remote_catalog = remote_catalog
        self.__print_input_settings()

    def upload_files(self):
        host = self.ftp_auth_data[0]
        username = self.ftp_auth_data[1]
        password = self.ftp_auth_data[2]
        ftp = FTP(host, username, password)
        # Задаём удаленный каталог
        ftp.cwd(self.remote_catalog)
        # Каждый файл из каталога загружаем на сервер
        file_list = os.listdir(self.local_catalog)
        print("Start uploading files...")
        for cnt in range(len(file_list)):
            file = open(self.local_catalog+"\\"+file_list[cnt], "rb")
            # Передаем файл на сервер
            send = ftp.storbinary("STOR " + file_list[cnt], file)
        # Закрываем FTP соединение
        ftp.close
        print("Files has been uploaded")


    def print_file_list(self):
        """Выводит в консоль список файлов в каталоге"""
        print("Files in catalog "+self.local_catalog)
        print(os.listdir(self.local_catalog))

    def __print_input_settings(self):
        print("Settings data in the Uploader")
        print(self.ftp_auth_data)
        print(self.local_catalog)
        print(self.remote_catalog)

